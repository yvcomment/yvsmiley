<?php
/*
* yvSmiley - A Smiley (emoticons) system extension (Plugin) for Joomla! 1.5
* @version	$Id: COPYRIGHT.php 0001 2007-07-07 07:37:00Z yvolk $
* @package 	yvSmiley
* @(c) 2007-2013 yvolk (Yuri Volkov), http://yurivolkov.com. All rights reserved.
* @license	GPL, see LICENSE.php
*/
?>

yvSmiley is copyrighted work of Yuri Volkov.
Copyright (c) 2007-2013 yvolk (Yuri Volkov), http://yurivolkov.com. All rights reserved.

yvSmiley includes or is derivative of works distributed under the following copyright notices:

Language packs
---
Copyright:	Translators of Language packs (to be expanded).
License:	GNU General Public License (GPL)

yvComment
---
Copyright (c) 2007-2013 yvolk (Yuri Volkov), http://yurivolkov.com. All rights reserved.
License:	GNU General Public License (GPL)

phpBB - Smiley images from phpBB
---
Copyright:	2007 phpBB Group, All Rights Reserved.
License:	GNU General Public License (GPL)

Joomla!
---
Copyright:	2005 - 2007 Open Source Matters. All rights reserved.
License:	GNU General Public License (GPL)
