<?php
/** 
* yvSmiley - Smiley Plugin for Joomla! 1.6
* This file contains changelog of this extension since v.1.03.001
* 
* @version		$Id: CHANGELOG.php 8 2013-02-09 12:16:25Z yvolk $
* @package		yvSmileyPlugin
* @copyright	2007-2013 yvolk (Yuri Volkov), http://yurivolkov.com. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
Legend:

* -> Security Fix
# -> Bug Fix
$ -> Language fix or change
+ -> Addition
^ -> Change
- -> Removed
! -> Note

v.2.1.000 09-February-2013 yvolk
 ^ Updated for Joomla! v.2.5. No "strict standards" warnings any more.

v.2.00.001 06-May-2011 yvolk
 # Fixed bug causing unnecessary '1' symbol shown on a page, see http://forum.joomla.org/viewtopic.php?f=579&t=617697

v.2.00.000 08-February-2011 yvolk
 ^ Modified by Bart Jochems (info@batjo.nl) and yvolk to run on Joomla! 1.6
 ^ Parameters that mention 'SectionIDs of Articles' were renamed to 'CategoryIDs...' because there are no 'Sections' in Joomla 1.6
 ^ PHP4 support dropped

v.1.03.001 25-August-2008 yvolk
 # PHP Notice referencing '$article->sectionid'
